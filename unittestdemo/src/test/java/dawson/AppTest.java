package dawson;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */

    @Test
    public void shouldEcho()
    {
        assertEquals("Testing echo method", 5, App.echo(5));
    }

    @Test
    public void shouldReturnOneMore()
    {
        assertEquals("Testing oneMore method", 6, App.oneMore(5));
    }
}
